const promise = new Promise(function(resolve, reject){

  const fs = require('fs')

  fs.readdir('/', (err, result) => {
    if (err) {
      reject(err.message)
    }
    resolve(console.log(result));
  })
})